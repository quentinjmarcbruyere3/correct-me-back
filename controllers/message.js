//Import
const { validationResult } = require("express-validator/check");
const Reverso = require("reverso-api");
const reverso = new Reverso();
const mariadb = require("mariadb");

// Create database connection
const pool = mariadb.createPool({
	host: "some-mariadb", 
	user:"example-user", 
	password: "my_cool_secret",
	database: "correctme"
});

// Function for send message
exports.sendMessage = (req, res) => {
	// Check erreur in request
	const errors = validationResult(req);
	if (!errors.isEmpty()) {
		// Send erreur request
		return res.status(422).json({
			message: "Validation failed, entered data is incorrect.",
			errors: errors.array()
		});
	}

	// Get all body information
	const auteur = req.body.auteur;
	const titre = req.body.titre;
	const text = req.body.texte;

	// Initialise error value
	let grammarError = 0;
	let spellingError = 0;

	// Call reverso api with the text
	reverso.getSpellCheck(text, "french", (err, response) => {
		if (err) throw new Error(err.message);

		// For each error on the response increment grammar or spelling error
		response.corrections.forEach(element => {
			if (element.type === "Grammar"){
				grammarError += 1;
			} else if (element.type === "Spelling"){
				spellingError += 1;
			}
		});

		// Save the result in the database
		pool.getConnection().then(conn => {
			conn.query("INSERT INTO text value (?,?,?,?,?,?)", [0, auteur, titre, text, grammarError, spellingError]).then(resp => {
				// Request the save result with the total of error
				conn.query("SELECT id, auteur, titre, texte, grammaire, orthographe, CAST((grammaire + orthographe) AS FLOAT) AS total FROM text WHERE id = " + resp.insertId).then(resp2 => {
					// Send response with the save
					conn.end();
					res.status(201).json({
						resp2
					});
				}).catch(err => {
					console.log(err);
				});
			}).catch(err => {
				console.log(err);
			});	
		}).catch(err => {
			console.log(err);
		});
	});
};
