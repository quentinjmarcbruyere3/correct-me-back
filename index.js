//Import
const express = require("express");
const messageRoute = require("./routes/message");
const bodyParser = require("body-parser");
const mariadb = require("mariadb");
const classementRoute = require("./routes/classement");

// Create app
const app = express();

// Import swagger
const swaggerUi = require("swagger-ui-express");
const swaggerDocument = require("./swagger.json");

// Parse body
app.use(bodyParser.json());

// Response header
app.use((req, res, next) => {
	res.setHeader("Access-Control-Allow-Origin", "*");
	res.setHeader("Access-Control-Allow-Methods", "OPTIONS, GET, POST, PUT, PATCH, DELETE");
	res.setHeader("Access-Control-Allow-Headers", "Content-Type, Authorization");
	next();
});

// Router redirect
app.use("/send", messageRoute);
app.use("/classement", classementRoute);

// Use swagger
app.use("/api-docs", swaggerUi.serve,  swaggerUi.setup(swaggerDocument));

// Use error log in the app
app.use((error, req, res) => {
	console.log(error);
	const status = error.statusCode || 500;
	const message = error.message;
	res.status(status).json({ message: message });
});

// Create database connection
const pool = mariadb.createPool({
	host: "some-mariadb", 
	user:"example-user", 
	password: "my_cool_secret",
	database: "correctme"
});

// Connect to database and create table
pool.getConnection().then(conn => {
	conn.query("CREATE TABLE IF NOT EXISTS text (id INT AUTO_INCREMENT PRIMARY KEY, auteur VARCHAR(255), titre VARCHAR(255), texte TEXT NOT NULL, grammaire INT, orthographe INT)").then(() => {
		// Start server if database is connected
		conn.end();
		app.listen(8080);
	}).catch((err) => {
		console.log(err);
	});	
}).catch(err => {
	console.log(err);
});