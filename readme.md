# Correct me (back)

It's the back-end of the application Correct me.

## Database command for start

### Start database localy

```
sudo docker run -p 3306:3306 --detach --name some-mariadb --env MARIADB_USER=example-user --env MARIADB_PASSWORD=my_cool_secret --env MARIADB_ROOT_PASSWORD=my-secret-pw --env MYSQL_DATABASE=correctme mariadb:latest
```

## Start back-end localy

```
node index.js
```

## Start all the application on the server

```
docker compose up -d
```
