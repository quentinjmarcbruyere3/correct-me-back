// Import
const express = require("express");
const classementController = require("../controllers/classement");

// Create router 
const router = express.Router();

// Router redirect
// GET /classement/general
router.get("/general", classementController.general);

// GET /classement/grammar
router.get("/grammaire", classementController.grammar);

// GET /classement/orthographe
router.get("/orthographe", classementController.orthographe);

// Export modules
module.exports = router;
