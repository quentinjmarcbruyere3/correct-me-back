//Import
const { validationResult } = require("express-validator/check");
const mariadb = require("mariadb");

// Create database connection
const pool = mariadb.createPool({
	host: "some-mariadb", 
	user:"example-user", 
	password: "my_cool_secret",
	database: "correctme"
});

// Function for get the general ranking
exports.general = (req, res) => {
	// Check erreur in request
	const errors = validationResult(req);
	if (!errors.isEmpty()) {
		// Send erreur request
		return res.status(422).json({
			message: "Validation failed, entered data is incorrect.",
			errors: errors.array()
		});
	}

	// Request database to get all record ordored by the number of total error
	pool.getConnection().then(conn => {
		conn.query("SELECT id, auteur, titre, texte, grammaire, orthographe, CAST((grammaire + orthographe) AS FLOAT) AS total FROM text ORDER BY total").then((resp) => {
			// Send the result
			conn.end();
			res.status(200).json({
				resp
			});
		}).catch((err) => {
			console.log(err);
		});	
	}).catch(err => {
		console.log(err);
	});
};

// Function for get the grammar ranking
exports.grammar = (req, res) => {
	// Check erreur in request
	const errors = validationResult(req);
	if (!errors.isEmpty()) {
		// Send erreur request
		return res.status(422).json({
			message: "Validation failed, entered data is incorrect.",
			errors: errors.array()
		});
	}

	// Request database to get all record ordored by the number of grammar error
	pool.getConnection().then(conn => {
		conn.query("SELECT id, auteur, titre, texte, grammaire, orthographe, CAST((grammaire + orthographe) AS FLOAT) AS total FROM text ORDER BY grammaire").then((resp) => {
			// Send the result
			conn.end();
			res.status(200).json({
				resp
			});
		}).catch((err) => {
			console.log(err);
		});	
	}).catch(err => {
		console.log(err);
	});
};

// Function for get the spelling ranking
exports.orthographe = (req, res) => {
	// Check erreur in request
	const errors = validationResult(req);
	if (!errors.isEmpty()) {
		// Send erreur request
		return res.status(422).json({
			message: "Validation failed, entered data is incorrect.",
			errors: errors.array()
		});
	}

	// Request database to get all record ordored by the number of spelling error
	pool.getConnection().then(conn => {
		conn.query("SELECT id, auteur, titre, texte, grammaire, orthographe, CAST((grammaire + orthographe) AS FLOAT) AS total FROM text ORDER BY orthographe").then((resp) => {
			// Send the result
			conn.end();
			res.status(200).json({
				resp
			});
		}).catch((err) => {
			console.log(err);
		});	
	}).catch(err => {
		console.log(err);
	});
};